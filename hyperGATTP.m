function hyperGATTP(instance_name, outfilename)


% Hyper heuristic for TTP_problem using a simple GA

global solsize_min;
global solsize_max;

%============================
% set GA params
%============================
gen = 500;
popsize = 10;
max_runtime = 600;
sel_rate = .9;

solsize_min = 3;
solsize_max = 64;

sol_in = 'sol_input.txt';
sol_out = 'sol_output.txt';


llh_set = char('kpsa','kpbf','tsp2opt','otspswap','otsp4opt','okpbf20','okpbf30','okpbf40');
llh_set = cellstr(llh_set);
nb_llh = size(llh_set,1); % number of heuristics




%============================
% TTP Initialization
%============================
% in Linux
% folder_name = strtok(char(instance_name), '_');
% command=['java -jar ttpapprox.jar database/TTP1_data/' folder_name '-ttp/ ' instance_name ' 71 10000 60000 SOMETHING ' initial_solution];
% [status res]=dos(command);

% in Windows:
sol_initial_file = strcat('initial-solutions/',instance_name,'_sol_input.txt');
arquivo = fopen(sol_initial_file);
sol_initial = fscanf(arquivo,'%c');
fclose(arquivo);
dlmwrite(sol_in,sol_initial,'delimiter',''); % re-write the initial solution


%============================
% initialize pop
%============================
tic
pop = zeros(popsize, solsize_max+2);
for i = 1:popsize
    solsize = randi([solsize_min, solsize_max]);
    sol = randi(nb_llh, 1, solsize);
    pop(i, 1:solsize) = sol;
    pop(i, solsize_max+1) = solsize;
    
    % get fitness
    pop(i, solsize_max+2) = ttpfitness(pop(i,:), llh_set, instance_name, sol_in, sol_out);
    dlmwrite(sol_in,sol_initial,'delimiter','');
end

[vmax,imax] = max(pop(:,solsize_max+2))



%============================
% GA main loop
%============================
offpopsize = sel_rate*popsize; % will be used later
offpop = zeros(popsize, solsize_max+2); % no need here..
for g=1:gen
    
    % check runtime limit
    toc;
    if toc>=max_runtime
        fprintf('MaxTime was reached !\n');
        break
    end
    
    % generate offsping population
    for k=1:2:popsize
        
%         % random selection of p1 and p2
%         i_p1 = randi(popsize);
%         i_p2 = randi(popsize);
%         while i_p2==i_p1
%             i_p2 = randi(popsize);
%         end
%         p1 = pop(i_p1,:);
%         p2 = pop(i_p2,:);
        
        % tournament
        pool_size=2;
        tour_size=3;
        pop_sel = tournament_selection(pop,pool_size,tour_size);
        p1=pop_sel(1,:);
        p2=pop_sel(2,:);
        
        % crossover operation
        [offspring, success] = std_crossover(p1, p2);
        c1=offspring(1,:);
        c1(solsize_max+2) = ttpfitness(c1, llh_set, instance_name, sol_in, sol_out);
        dlmwrite(sol_in,sol_initial,'delimiter','');
        c2=offspring(2,:);
        c2(solsize_max+2) = ttpfitness(c2, llh_set, instance_name, sol_in, sol_out);
        dlmwrite(sol_in,sol_initial,'delimiter','');
        
        offpop(k,:) = c1;
        offpop(k+1,:) = c2;
    end
    
    % merge populations, keep best (~ 10%)
    i=1;
    % best solution
    [vmax,imax] = max(pop(:,solsize_max+2));
    for k=1:popsize
        if i==imax
            i=i+1;
        end
        if i>popsize
            break
        end
        pop(i,:) = offpop(k,:);
        i=i+1;
    end
    
end

% get best solution in pop
[vmax,imax] = max(pop(:,solsize_max+2));


% save output in csv file
fileID = fopen(outfilename,'a');
fprintf(fileID, '%s %d\n', instance_name, vmax);
fclose(fileID);
