function obj_value = ttpfitness(hhsolution, heuristic, instance_name, sol_in, sol_out);
global solsize_max;

j=1;
solsize = hhsolution(solsize_max+1);
while (j < solsize)
    command=['java -jar ttplab-bbox.jar ' char(heuristic(hhsolution(j))) ' ' instance_name ' ' sol_in ' ' sol_out];
    [status,result]=dos(command);
    teste=sol_in;
    arquivo = fopen(teste);
    input = fscanf(arquivo,'%c');
    fclose(arquivo);
    teste=sol_out; %precisa ler o output sem a primeira linha
    arquivo = fopen(teste);
    output = fscanf(arquivo,'%c');
    fclose(arquivo);
    [b solution]=strtok(output,'_');
    [header b]=strtok(input,'_');
    space=find(result==' ');
    obj_value=str2num(result(space(1)+1:space(2)-1));
    %fprintf('obj_value=%f. j:%d, heuristic=%s \n',obj_value,j,char(heuristic(hhsolution(i,j))));
    input=strcat(header,solution);
    %new input file will be the current output
    input=strcat(header,solution);
    dlmwrite(sol_in,input,'delimiter','');%write input in txt file
    j=j+1;
end
%hhsolution(i,solsize_max+2)=obj_value;%fitness value from last heuristic applied
%fprintf('fitness_individual %d=%f. \n',i,obj_value);
