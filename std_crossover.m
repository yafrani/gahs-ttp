function [offspring, success] = std_crossover(p1, p2)

global solsize_max;

%p1
%p2

p1_size = p1(solsize_max+1);
p2_size = p2(solsize_max+1);

p1_xpoint = randi([2,p1_size-1]);
p2_xpoint = randi([2,p2_size-1]);
%[p1_xpoint p2_xpoint]

% offspring sizes
c1_size = p1_xpoint + (p2_size-p2_xpoint);
c2_size = p2_xpoint + (p1_size-p1_xpoint);
%[c1_size c2_size]

if c1_size > solsize_max || c2_size > solsize_max
    offspring = [p1; p2];
    success = 0;
    return
end

c1 = zeros(1,solsize_max+2);
c2 = zeros(1,solsize_max+2);
c1(1:c1_size) = [ p1(1:p1_xpoint) p2( p2_xpoint+1:p2_size) ];
c2(1:c2_size) = [ p2(1:p2_xpoint) p1( p1_xpoint+1:p1_size) ];
c1(solsize_max+1) = c1_size;
c2(solsize_max+1) = c2_size;

offspring = [c1; c2];
success = 1;
