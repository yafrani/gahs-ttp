% run many instances

file = 'instances_online.txt';
arquivo = fopen(file);
instances = fscanf(arquivo,'%c');
fclose(arquivo);
instances = strread(instances,'%s');

outfilename='output/results-GA.csv';

% nb of instances
nb_inst = size(instances,1);

% nb of repetitions
nb_rep = 10

% loop & run all instances
for i = 1:nb_inst
    for j = 1:nb_rep
        % current instance
        instance_name = instances{i};
        display(['===>' instance_name]);
        
        % run for instance_name
        hyperGATTP(instance_name, outfilename)
        pause(5.0);
    end
end
